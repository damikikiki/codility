package solution.Lesson4;

import java.util.Arrays;

public class MissingInteger {
    /*
    Q : https://app.codility.com/programmers/lessons/3-time_complexity/perm_missing_elem/
    Detected time complexity:
    O(N) or O(N * log(N))

    TaskScore : 100%
    Correctness : 100%
    Performance : 100%
*/
    public int solution(int[] A) {
        int size = A.length;
        int[] result = new int[size];
        for(int i :A){
            if(i > A.length||i<=0) continue;
            result [i-1] = 1;
        }
        for(int i = 0; i<size; i++){
            if(result[i]==0) return i+1;
        }
        return size+1;
    }
}
