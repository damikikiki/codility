package solution.Lesson3;

public class TapeEquilibrium {
    /*
    Q : https://app.codility.com/programmers/lessons/3-time_complexity/perm_missing_elem/
    Detected time complexity:
    O(N)

    TaskScore : 100%
    Correctness : 100%
    Performance : 100%
*/
    public int solution(int[] A) {
        int sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum += A[i];
        }
        int min = Integer.MAX_VALUE;
        for (int i = A.length - 1; i > 0; i--) {
            sum -= 2 * A[i];
            min = Math.min(min, Math.abs(sum));
        }
        return min;
    }
}
