package solution.Lesson3;

public class FrogJmp {
    /*
    Q : https://app.codility.com/programmers/lessons/3-time_complexity/frog_jmp/
    Detected time complexity:
    O(1)

    TaskScore : 100%
    Correctness : 100%
    Performance : 100%

 */
    public int solution(int X, int Y, int D) {
        return ((Y-X)%D==0)?((Y-X)/D)+1:((Y-X)/D)+1;
    }
}
