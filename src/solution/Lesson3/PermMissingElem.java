package solution.Lesson3;

public class PermMissingElem {
      /*
    Q : https://app.codility.com/programmers/lessons/3-time_complexity/perm_missing_elem/
    Detected time complexity:
    O(N) or O(N * log(N))

    TaskScore : 100%
    Correctness : 100%
    Performance : 100%
 */
      public int solution(int[] A) {
          int sum = 0;
          int i;
          for(i = 0 ; i < A.length; ){
              sum = sum - A[i] + ++i;
          }
          return sum + ++i;
      }
}
