package solution.Lesson1;

public class BinaryGap {
    public int solution(int N) {
        boolean start = false;
        int max = 0;
        int count = 0;
        for (; N / 2 != 0; N /= 2) {
            if ((N % 2) == 0) {
                if (start) count++;
            } else {
                start = true;
                if (max < count) max = count;
                count = 0;
            }
        }
        if (max < count) max = count;
        return max;
    }
}
