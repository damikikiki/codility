package solution.Lesson2;

import java.util.HashSet;
import java.util.Set;

/*
    Q : https://app.codility.com/programmers/lessons/2-arrays/odd_occurrences_in_array/
    Detected time complexity:
    O(N) or O(N*log(N))

    TaskScore : 100%
    Correctness : 100%
    Performance : 100%

 */
public class OddOccurrencesInArray {
    public int solution(int[] A) {
      int size = A.length;
      int i;
      Set<Integer> set = new HashSet<>();
        for(i = 0;i<size;i++){
            if(set.contains(A[i])){
                set.remove(A[i]);
            }else{
                set.add(A[i]);
            }
        }
      return set.stream().findFirst().get();
    }
}
