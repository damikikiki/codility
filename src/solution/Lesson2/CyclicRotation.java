package solution.Lesson2;

class CyclicRotation {
    public int[] solution(int[] A, int K) {
        int size = A.length;
        int[] B = new int[size];
        for(int i = 0;i < size; i++){
            B[(i+K)%size] = A[i];
        }
        return B;
    }
}
